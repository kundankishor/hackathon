"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var hero_service_1 = require("../hero.service");
var JourneyComponent = (function () {
    function JourneyComponent(heroService) {
        this.heroService = heroService;
    }
    JourneyComponent.prototype.ngOnInit = function () {
        this.getHeroes();
    };
    JourneyComponent.prototype.getHeroes = function () {
        var _this = this;
        this.heroService.getHeroes()
            .subscribe(function (heroes) { return _this.heroes = heroes; });
    };
    return JourneyComponent;
}());
JourneyComponent = __decorate([
    core_1.Component({
        selector: 'app-journey',
        templateUrl: './journey.component.html',
        styleUrls: ['./journey.component.css']
    }),
    __metadata("design:paramtypes", [hero_service_1.HeroService])
], JourneyComponent);
exports.JourneyComponent = JourneyComponent;
//# sourceMappingURL=journey.component.js.map