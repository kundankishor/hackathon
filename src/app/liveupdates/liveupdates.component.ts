import { MessageService } from './../data/message.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-liveupdates',
  templateUrl: './liveupdates.component.html',
  styleUrls: ['./liveupdates.component.css']
})
export class LiveUpdatesComponent implements OnInit {

  constructor(public messageService: MessageService) { }

  ngOnInit() {
  }

}
