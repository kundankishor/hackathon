import { TrackingRecord } from './../data/trackingReocrd';
import { TrackingService } from './../tracking.service';
import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';

@Component({
  selector: 'app-timeline-detail',
  templateUrl: './timeline-detail.component.html',
  styleUrls: ['./timeline-detail.component.css']
})
export class TimelineDetailComponent implements OnInit {

  record: TrackingRecord;

  constructor(private trackingService: TrackingService, private route: ActivatedRoute, private toast: MatSnackBar, private location: Location) { }

  ngOnInit() {
    this.record = this.trackingService.getTrackingRecord(this.route.snapshot.params['pnr']);
    if (this.record == undefined) {
      this.toast.open('PNR not found.', 'OK');
      this.location.back();
    }
    if (this.record.raiseNotification) {
      this.toast.open('It seems you have missed boarding. We have initiated baggage offload.', 'OK');
    }
  }

}
