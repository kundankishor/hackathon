import { HeroService } from './data/hero.service';
import { LiveUpdatesComponent } from './liveupdates/liveupdates.component';
import { TrackingService } from './tracking.service';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxCarouselModule } from 'ngx-carousel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import 'hammerjs';


import { AppComponent } from './app.component';
import { TimelineComponent } from './timeline/timeline.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { TimelineDetailComponent } from './timeline-detail/timeline-detail.component';
import { MatSnackBarModule } from '@angular/material';
import { journeyDetailComponent } from './journey-detail/journey-detail.component';
import { JourneyComponent } from './journey/journey.component';
import { NewtripComponent } from './newtrip/newtrip.component';
import { MessageService } from './data/message.service';



@NgModule({
  declarations: [
    AppComponent,
    TimelineComponent,
    HomeComponent,
    TimelineDetailComponent,
    journeyDetailComponent,
    JourneyComponent,
    NewtripComponent,
    LiveUpdatesComponent
  ],
  imports: [
    BrowserModule, NgxCarouselModule, AppRoutingModule, FormsModule, BrowserAnimationsModule, MatSnackBarModule
  ],
  providers: [TrackingService, HeroService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
