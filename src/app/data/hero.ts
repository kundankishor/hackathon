export class Hero {
  id: number;
  name: string;
  description:string;
  startDate:string;
  endDate:string;
}
