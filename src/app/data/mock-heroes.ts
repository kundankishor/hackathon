import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 1, name: 'Exploring London',description:'Another Great Adventure',startDate:'22-Dec-2017',endDate:'1-Jan-2018'},
  { id: 2, name: 'Austria Memories' ,description:'Another Great Adventure',startDate:'22-Dec-2017',endDate:'1-Jan-2018'},
  { id: 3, name: 'Evening in Frankfurt',description:'Another Great Adventure',startDate:'22-Dec-2017',endDate:'1-Jan-2018'},
  { id: 4, name: 'My France Trip' ,description:'Another Great Adventure',startDate:'22-Dec-2017',endDate:'1-Jan-2018'}
];
