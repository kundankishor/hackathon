import { TrackingRecord } from './trackingReocrd';
export const TrackingRecords: TrackingRecord[] = [
    {
        pnr: 'LI5XUS',
        origin: 'DXB (Dubai International Airport)',
        destination: 'LHR (London Heathrow Airport)',
        plannedCheckin: '27-Jan-2018 07:15:00',
        actualCheckin: '27-Jan-2018 07:39:00',
        plannedBoarding: '27-Jan-2018 10:15:00',
        actualBoarding: '27-Jan-2018 10:12:00',
        plannedDeparture: '27-Jan-2018 10:40:00',
        actualDeparture: '27-Jan-2018 10:41:00',
        plannedArrival: '27-Jan-2018 16:15:00',
        actualArrival: '27-Jan-2018 16:12:00',
        pax: [
            'Mr Kundan Kishor', 'Mrs Vartika Kumari', 'Ms Aanya Kashyap', 'Mr Aadvik Kashyap'
        ],
        raiseNotification: false,
        flightDate: '27-Jan-2018',
        flightNumber: 'EK-0007',
        baggage: { tags: ['EK588969', 'EK342687', 'EK537476'], pieces: 3, weight: 78 }
    },
    {
        pnr: 'GURAG6',
        origin: 'DXB (Dubai International Airport)',
        destination: 'DEL (Indira Gandhi International Airport)',
        plannedCheckin: '27-Jan-2018 16:10:00',
        actualCheckin: '27-Jan-2018 07:39:00',
        plannedBoarding: '27-Jan-2018 19:15:00',
        actualBoarding: 'Yet to be updated',
        plannedDeparture: '27-Jan-2018 19:40:00',
        actualDeparture: 'Yet to be updated',
        plannedArrival: '29-Jan-2018 00:30:00',
        actualArrival: 'Yet to be updated',
        pax: [
            'Mr Rishi Mishra', 'Mrs Rishi Mishra'
        ],
        raiseNotification: true,
        flightDate: '27-Jan-2018',
        flightNumber: 'EK-510',
        baggage: { tags: ['EK213241', 'EK234231'], pieces: 2, weight: 39 }
    },
    {
        pnr: 'RMALL8',
        origin: 'DXB (Dubai International Airport)',
        destination: 'BLR (Kempegowda International Airport)',
        plannedCheckin: '27-Jan-2018 16:10:00',
        actualCheckin: 'Yet to be updated',
        plannedBoarding: '27-Jan-2018 19:15:00',
        actualBoarding: 'Yet to be updated',
        plannedDeparture: '27-Jan-2018 19:40:00',
        actualDeparture: 'Yet to be updated',
        plannedArrival: '29-Jan-2018 00:30:00',
        actualArrival: 'Yet to be updated',
        pax: [
            'Mr Alec Sadler', 'Mrs Alec Sadler'
        ],
        raiseNotification: false,
        flightDate: '27-Jan-2018',
        flightNumber: 'EK-567',
        baggage: {}
    }
];