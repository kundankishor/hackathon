import { LiveUpdatesComponent } from './../liveupdates/liveupdates.component';
import { TimelineDetailComponent } from './../timeline-detail/timeline-detail.component';
import { TimelineComponent } from './../timeline/timeline.component';
import { HomeComponent } from './../home/home.component';
import { Routes } from '@angular/router';
import { journeyDetailComponent } from '../journey-detail/journey-detail.component';
import { JourneyComponent } from '../journey/journey.component';
import { NewtripComponent } from '../newtrip/newtrip.component';

export const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'timeline', component: TimelineComponent },
    { path: 'timeline/:pnr', component: TimelineDetailComponent },
    { path: 'detail/:id', component: journeyDetailComponent },
    { path: 'journey', component: JourneyComponent },
    { path: 'newtrip', component: NewtripComponent },
    { path: 'liveupdates', component: LiveUpdatesComponent },
    { path: '', redirectTo: '/home', pathMatch: 'full' }
];