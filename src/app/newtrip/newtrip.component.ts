import { MessageService } from './../data/message.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-trip',
  templateUrl: './newtrip.component.html',
  styleUrls: ['./newtrip.component.css']
})
export class NewtripComponent implements OnInit {

  constructor(public messageService: MessageService) { }

  ngOnInit() {
  }

}
